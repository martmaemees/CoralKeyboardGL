#version 330 core

uniform sampler2D gradientTexture;

in float vDensity;

out vec4 fragColor;

void main() {
    float d = vDensity * 1.0;
//    fragColor = texture(gradientTexture, vec2(128, 128));
//    fragColor = texture(gradientTexture, vec2(clamp(d*255, 0, 255), 0.0));
     fragColor = vec4(d-2.0, d-1.0, d, 1.0);
//    fragColor = vec4(1.0, 0.0, 0.0, 1.0);
}