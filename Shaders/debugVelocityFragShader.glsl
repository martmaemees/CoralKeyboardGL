#version 330 core

in vec2 vVelocity;

out vec4 fragColor;

void main()
{
    fragColor = vec4(vVelocity, 1.0, 1.0);
}