#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 velocity;

out vec2 vVelocity;

void main() {
    gl_Position = position;
    vVelocity = velocity;
}