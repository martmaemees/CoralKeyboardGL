package FluidSystem;

public class Vec2i {
    public int x, y;

    public Vec2i(int x, int y) { this.x = x; this.y = y; }

    public void sub(Vec2i v) {
        this.x -= v.x;
        this.y -= v.y;
    }

    public Vec2i subN(Vec2i v) {
        return new Vec2i(x-v.x, y-v.y);
    }
}
