import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public abstract class GameContext implements IContext {

    protected int windowWidth = 800;
    protected int windowHeight = 800;
    private String title = "Game";
    protected long window;

    private GLFWErrorCallback errorCallback;
    private GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                System.out.println("Closing the window.");
                glfwSetWindowShouldClose(window, true);
            }
        }
    };

    public GameContext(int width, int height, String title) {
        this.windowWidth = width;
        this.windowHeight = height;
        this.title = title;
    }

    @Override
    public void init() {
        errorCallback = GLFWErrorCallback.createPrint(System.err);
        glfwSetErrorCallback(errorCallback);

        if(!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW for the context.");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
        glfwWindowHint(GLFW_STENCIL_BITS, 4);
        glfwWindowHint(GLFW_SAMPLES, 4);

        window = glfwCreateWindow(windowWidth, windowHeight, title, NULL, NULL);
        if(window == NULL) {
            glfwTerminate();
            throw new RuntimeException("Failed to create the GLFW window for the context.");
        }
        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        GL11.glClearColor(0.8f, 0.0f, 0.8f, 1.0f);

        glfwSetKeyCallback(window, keyCallback);

        initialize();
    }

    @Override
    public void gameLoop() {
        while(!glfwWindowShouldClose(window)) {
            update();
            render();
            glfwSwapBuffers(window);
            glfwPollEvents();
        }
    }

    @Override
    public void dispose() {
        glfwDestroyWindow(window);
        keyCallback.free();

        glfwTerminate();
        errorCallback.free();
    }

    @Override
    public abstract void update();

    @Override
    public abstract void render();

    @Override
    public abstract void initialize();

    @Override
    public void startGame() {
        try {
            init();
            gameLoop();
        }
        finally {
            dispose();
        }
    }

    @Override
    public void setKeyCallback(GLFWKeyCallback value) {
        keyCallback.free();
        keyCallback = value;
        glfwSetKeyCallback(window, keyCallback);
    }

}
