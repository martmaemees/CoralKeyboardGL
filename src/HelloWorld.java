import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWKeyCallbackI;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class HelloWorld {

    public static void main(String[] args) {
        FluidContext fluidContext = new FluidContext(800, 800, "Fluid", 256);
        fluidContext.startGame();
    }
}
