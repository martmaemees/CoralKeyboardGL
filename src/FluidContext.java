import FluidSystem.FluidMatrix;
import FluidSystem.Vec2d;
import FluidSystem.Vec2i;
import de.matthiasmann.twl.utils.PNGDecoder;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_FLOAT;

public class FluidContext extends GameContext {

    /*
     TODO: Add a gradient, to the renderer.
     TODO: Calculate the line of mouse movement between frames.
     TODO: Optimize somehow?
     */

    // DEBUGGING FLAGS
    private boolean OUTPUT_Y_VELOCITY = false;

    private FluidMatrix fluidMatrix;
    private int size;
    float[] vertexData;
    int[] triangleIndices;
    ShaderProgram mainShader;
    private FloatBuffer vboBuffer;
    private IntBuffer vboIBuffer;
    private int vao;
    private int vbo;
    private int vboI;
    private int texId;
    private double lastTime = 0.0;
    private double deltaTime = 0.0;
    private double timeMultiplier = 1.0;
    private double densitySourceStrength = 80.0;
    private double velocitySourceStrength = 40.0;
    private DoubleBuffer cursorX;
    private DoubleBuffer cursorY;
    private Vec2i lastMousePosition = null;

    private double densitySourceActiveTime = 0.75;
    private double velocitySourceActiveTime = 0.5;
    private HashSet<ActiveTimer> activeDensitySources = new HashSet<>();
    private HashSet<ActiveTimer> activeVelocitySources = new HashSet<>();

    public FluidContext(int width, int height, String title, int size) {
        super(width, height, title);
        this.size = size;
        vertexData = new float[size*size*4];
        triangleIndices = new int[(size-1)*(size-1)*2*3];
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                vertexData[(i*size + j)*4] = ((float)i / (size-1))*2-1;
                vertexData[(i*size + j)*4+1] = ((float)j / (size-1))*2-1;
                vertexData[(i*size + j)*4+2] = 1.0f;
            }
        }
        int ti = 0; // tri index
        for(int y = 0; y < size-1; y++) {
            for(int x = 0; x < size-1; x++) {
                triangleIndices[ti] = x + y*size;
                triangleIndices[ti+1] = triangleIndices[ti+4] = (x+1) + y*size;
                triangleIndices[ti+2] = triangleIndices[ti+3] = x + (y+1)*size;
                triangleIndices[ti+5] = (x+1) + (y+1)*size;
                ti += 6;
            }
        }

        fluidMatrix = new FluidMatrix(new int[] {size, size});
    }

    @Override
    public void initialize() {
        try {
            mainShader = new ShaderProgram("./Shaders/vertShader.glsl", "./Shaders/fragShader.glsl");
            mainShader.use();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ByteBuffer texBuffer = null;
        int tWidth = 0;
        int tHeight = 0;

        InputStream in = null;
        try {
            in = new FileInputStream("./Textures/gradient.png");
            PNGDecoder decoder = new PNGDecoder(in);

            tWidth = decoder.getWidth();
            tHeight = decoder.getHeight();

            texBuffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(texBuffer, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            texBuffer.flip();

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        texId = GL11.glGenTextures();
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);

        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, tWidth, tHeight, 0,
                GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, texBuffer);
         GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
        GL20.glUniform1i(GL20.glGetUniformLocation(mainShader.getShaderProgram(), "gradientTexture"), GL13.GL_TEXTURE0);

        cursorX = BufferUtils.createDoubleBuffer(1);
        cursorY = BufferUtils.createDoubleBuffer(1);

        vboBuffer = MemoryUtil.memAllocFloat(vertexData.length);
        vboIBuffer = MemoryUtil.memAllocInt(triangleIndices.length);

        vboBuffer.put(vertexData);
        vboBuffer.flip();

        vboIBuffer.put(triangleIndices);
        vboIBuffer.flip();

        vao = GL30.glGenVertexArrays();
        vbo = GL15.glGenBuffers();
        vboI = GL15.glGenBuffers();
        GL30.glBindVertexArray(vao);

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboI);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, vboIBuffer, GL15.GL_STATIC_DRAW);

        MemoryUtil.memFree(vboIBuffer);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
        int posAttrib = GL20.glGetAttribLocation(mainShader.getShaderProgram(), "position");
        GL20.glEnableVertexAttribArray(posAttrib);
        GL20.glVertexAttribPointer(posAttrib, 3, GL_FLOAT, false, 4 * Float.BYTES, 0);

        int denAttrib = GL20.glGetAttribLocation(mainShader.getShaderProgram(), "density");
        GL20.glEnableVertexAttribArray(denAttrib);
        GL20.glVertexAttribPointer(denAttrib, 1, GL_FLOAT, false, 4 * Float.BYTES, 3 * Float.BYTES);
    }

    @Override
    public void update() {
        deltaTime = glfwGetTime() - lastTime;
        lastTime = glfwGetTime();
//        System.out.println(deltaTime);

        if(OUTPUT_Y_VELOCITY) {
            for(double[] vr : fluidMatrix.getVelocityY()) {
                for(double vy : vr)
                    System.out.print(vy+"\t");
                System.out.println("");
            }
        }

        glfwGetCursorPos(window, cursorX, cursorY);
        int mouseX = (int)Math.round(MathHelpers.clamp(cursorX.get(0), 0, windowWidth) * ((double)size/(double)windowWidth));
        int mouseY = (int)Math.round((windowHeight - MathHelpers.clamp(cursorY.get(0), 0, windowHeight)) * ((double)size/(double)windowHeight));
        Vec2i newPos = new Vec2i(mouseX, mouseY);
        HashSet<Vec2i> pointsCrossedByMouse = new HashSet<>();
        if(lastMousePosition == null)
            lastMousePosition = new Vec2i(mouseX, mouseY);
        Vec2i deltaPos = newPos.subN(lastMousePosition);

        processDensityInput(pointsCrossedByMouse);
        processVelocityInput(pointsCrossedByMouse);

        fluidMatrix.timeStep(deltaTime*timeMultiplier);

        // Buffering the density data to the GPU.
        bufferDensityData();
    }

    @Override
    public void render() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

        GL30.glBindVertexArray(vao);
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboI);

        GL11.glDrawElements(GL11.GL_TRIANGLES, triangleIndices.length, GL11.GL_UNSIGNED_INT, 0);
    }

    @Override
    public void dispose() {
        GL15.glDeleteBuffers(vbo);
        GL15.glDeleteBuffers(vboI);
        GL30.glDeleteVertexArrays(vao);
        GL11.glDeleteTextures(texId);

        MemoryUtil.memFree(vboBuffer);
        super.dispose();
    }

    protected void processDensityInput(HashSet<Vec2i> inputPoints) {
        Iterator<ActiveTimer> iter = activeDensitySources.iterator();

        while(iter.hasNext()) {
            ActiveTimer denSource = iter.next();
            denSource.time += deltaTime;
            if(denSource.time >= densitySourceActiveTime) {
                fluidMatrix.clearDensitySource(denSource.x, denSource.y);
                iter.remove();
            }
        }

        if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_TRUE) {
            glfwGetCursorPos(window, cursorX, cursorY);

            int x = (int)Math.round(MathHelpers.clamp(cursorX.get(0), 0, windowWidth) * ((double)size/(double)windowWidth));
            int y = (int)Math.round((windowHeight - MathHelpers.clamp(cursorY.get(0), 0, windowHeight)) * ((double)size/(double)windowHeight));
            if(!activeDensitySources.contains(new ActiveTimer(x, y))) {
                activeDensitySources.add(new ActiveTimer(x, y));
                fluidMatrix.addDensitySource(x, y, densitySourceStrength);
            }
        }
    }

    protected void processVelocityInput(HashSet<Vec2i> inputPoints) {
        Iterator<ActiveTimer> iter = activeVelocitySources.iterator();

        while(iter.hasNext()) {
            ActiveTimer velSource = iter.next();
            velSource.time += deltaTime;
            if(velSource.time >= velocitySourceActiveTime) {
                fluidMatrix.clearVelocitySource(velSource.x, velSource.y);
                iter.remove();
            }
        }

        if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_TRUE) {
            glfwGetCursorPos(window, cursorX, cursorY);

            int x = (int)Math.round(MathHelpers.clamp(cursorX.get(0), 0, windowWidth) * ((double)size/(double)windowWidth));
            int y = (int)Math.round((windowHeight - MathHelpers.clamp(cursorY.get(0), 0, windowHeight)) * ((double)size/(double)windowHeight));

            if( x > 0 && !activeVelocitySources.contains(new ActiveTimer(x-1, y))) {
                activeVelocitySources.add(new ActiveTimer(x-1, y));
                fluidMatrix.addVelocitySource(x-1, y, new Vec2d(-velocitySourceStrength, 0.0));
            }

            if( x < size && !activeVelocitySources.contains(new ActiveTimer(x+1, y))) {
                activeVelocitySources.add(new ActiveTimer(x+1, y));
                fluidMatrix.addVelocitySource(x+1, y, new Vec2d(velocitySourceStrength, 0.0));
            }

            if( y < 0 && !activeVelocitySources.contains(new ActiveTimer(x, y-1))) {
                activeVelocitySources.add(new ActiveTimer(x, y-1));
                fluidMatrix.addVelocitySource(x, y-1, new Vec2d(0.0, -velocitySourceStrength));
            }

            if( y > size && !activeVelocitySources.contains(new ActiveTimer(x, y+1))) {
                activeVelocitySources.add(new ActiveTimer(x, y+1));
                fluidMatrix.addVelocitySource(x, y+1, new Vec2d(0.0, velocitySourceStrength));
            }
        }
    }

    private void bufferDensityData() {
        double[][] fluid = fluidMatrix.getRho();
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                vboBuffer.put((i*size+j)*4+3, (float)fluid[i+1][j+1]); // +1 because .getRho() gets the boundary as well.
            }
        }

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vboBuffer, GL15.GL_STATIC_DRAW);
    }
}

class ActiveTimer {
    int x, y;
    double time;

    public ActiveTimer(int x, int y) { this.x = x; this.y = y; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActiveTimer that = (ActiveTimer) o;

        if (x != that.x) return false;
        return y == that.y;
        }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}