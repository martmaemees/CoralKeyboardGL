public class MathHelpers {
    public static double clamp(double a, double min, double max) {
        if(a < min)
            return min;
        if(a > max)
            return max;
        return a;
    }

    public static int clamp(int a, int min, int max) {
        if(a < min)
            return min;
        if(a > max)
            return max;
        return a;
    }
}
