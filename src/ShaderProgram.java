import org.lwjgl.opengl.GL20;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.lwjgl.opengl.GL11.GL_TRUE;

public class ShaderProgram {
    private int vertShader;
    private int fragShader;
    private int shaderProgram;

    public ShaderProgram(String vertShaderPath, String fragShaderPath) throws FileNotFoundException {

        vertShader = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
        GL20.glShaderSource(vertShader, LoadShaderSource(vertShaderPath));
        GL20.glCompileShader(vertShader);

        fragShader = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
        GL20.glShaderSource(fragShader, LoadShaderSource(fragShaderPath));
        GL20.glCompileShader(fragShader);

        int status;
        status = GL20.glGetShaderi(vertShader, GL20.GL_COMPILE_STATUS);
        if(status != GL_TRUE) {
            System.out.println(GL20.glGetShaderInfoLog(vertShader));
            throw new RuntimeException(GL20.glGetShaderInfoLog(vertShader));
        }

        status = GL20.glGetShaderi(fragShader, GL20.GL_COMPILE_STATUS);
        if(status != GL_TRUE) {
            System.out.println(GL20.glGetShaderInfoLog(fragShader));
            throw new RuntimeException(GL20.glGetShaderInfoLog(fragShader));
        }

        shaderProgram = GL20.glCreateProgram();
        GL20.glAttachShader(shaderProgram, vertShader);
        GL20.glAttachShader(shaderProgram, fragShader);
        GL20.glLinkProgram(shaderProgram);

        status = GL20.glGetProgrami(shaderProgram, GL20.GL_COMPILE_STATUS);
        if(status != GL_TRUE) {
            System.out.println(GL20.glGetProgramInfoLog(shaderProgram));
//            throw new RuntimeException(GL20.glGetProgramInfoLog(shaderProgram));
        }
    }

    private String LoadShaderSource(String path) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(path));
        String s = "";
        while(sc.hasNextLine()) {
            s += sc.nextLine() + "\n";
        }
        return s;
    }

    public void use() { GL20.glUseProgram(shaderProgram); }

    public int getShaderProgram() {
        return shaderProgram;
    }
}
