import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;

public interface IContext {

    void init();
    void gameLoop();
    void dispose();

    void update();
    void render();
    void initialize();

    void startGame();

    void setKeyCallback(GLFWKeyCallback value);
}
