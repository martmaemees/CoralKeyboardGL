# CoralKeyboardGL

### This project has been re-written [HERE](https://gitlab.com/martmaemees/FDSim)

## Introduction

This is a Fluid Dynamics Simulation written in Java, using OpenGL through LWJGL for rendering. Built on a school project [CoralKeyboard](https://gitlab.com/martmaemees/CoralKeyboard) created by me ([Märt Mäemees](https://gitlab.com/martmaemees)) and [Hendrik Eerikson](https://gitlab.com/hendrik.eerikson)

The algorithm for the fluid dynamics is from [Real-Time Fluid Dynamics for Games by Jos Stam](http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf)

<img src="http://kodu.ut.ee/~mmaemees/media/2017-12-21_21-14-51.gif" width="600" height="600">

## Dependencies

[LWJGL 3](https://www.lwjgl.org/) (3.1.3 build 16 was used in development)

[Java OpenGL Mathematics Library](https://github.com/jroyalty/jglm)

[TWL PNGDecoder](http://twl.l33tlabs.org/dist/PNGDecoder.jar)

## Running

* Download the [Latest Build](http://kodu.ut.ee/~mmaemees/downloads/CoralKeyboard.zip)
* Unpack the archive into a folder
* Run the .jar file in the folder, with the "Textures" and "Shaders" folders in the same directory